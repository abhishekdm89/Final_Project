package com.tmobile.interfaces;

public abstract class PlanType {
	public ContractType contractType;
	public PlanType() {
		
	}
	public PlanType(ContractType contractType) {
		this.contractType=contractType;
	}
	
public abstract double perMonth(int lines);
public abstract double perMonth(int lines,double deviceCost);

}
