package com.tmobile.services;

import com.tmobile.interfaces.ContractType;

public class TwelveMonth implements ContractType {

	@Override
	public double deviceMonthlyCost(double cost) {
		int months=12;
		double monthlyCost =cost/months;
		
		return monthlyCost;
	}

}
