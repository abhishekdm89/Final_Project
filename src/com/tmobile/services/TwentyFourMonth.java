package com.tmobile.services;

import com.tmobile.interfaces.ContractType;

public class TwentyFourMonth implements ContractType {

	@Override
	public double deviceMonthlyCost(double cost) {
		int months=24;
		double interest = 1.001;
		double monthlyCost = cost/months*interest;
		
		return monthlyCost;
	}
	

}
