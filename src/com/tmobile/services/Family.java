package com.tmobile.services;

import com.tmobile.interfaces.ContractType;
import com.tmobile.interfaces.PlanType;

public class Family extends PlanType {
	public Family() {
		
	}

	public Family(ContractType contractType) {
		super(contractType);

	}

	@Override
	public double perMonth(int lines) {
		double price = 70;
		if (lines == 2) {
			price = price * 0.8 * 2;
		} else if (lines == 3) {
			price = price * 0.7 * 3;
		} else if (lines == 4) {
			price = price * 0.5 * 4;
		} else {
			throw new IllegalArgumentException("Number of lines should be between 2 and 4");
		} 
		System.out.println("without the phone for :"+ lines+" "+price);
		
		return price;
	}
	@Override
	public double perMonth(int lines, double deviceCost) {
		double totalPrice;
		System.out.println("before adding device"+perMonth(lines));
		totalPrice = perMonth(lines) + contractType.deviceMonthlyCost(deviceCost);
		System.out.println("with the phone :"+totalPrice);
		return totalPrice;

	}

}
