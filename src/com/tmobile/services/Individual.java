package com.tmobile.services;

import com.tmobile.interfaces.ContractType;
import com.tmobile.interfaces.PlanType;

public class Individual extends PlanType{
	
	public Individual() {
		
	}
	public Individual(ContractType contractType) {
		super(contractType);
	}

	@Override
	public double perMonth(int lines) {
		double price=70;
		return price;
	}
	
	@Override
	public double perMonth(int lines, double cost) {
		double totalPrice;
		totalPrice = perMonth(lines) + contractType.deviceMonthlyCost(cost);
		System.out.println("with the phone :"+totalPrice);
		return totalPrice;
	}

}
