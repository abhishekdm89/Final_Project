package com.tmobile.objectsfactory;

import com.tmobile.interfaces.ContractType;
import com.tmobile.interfaces.PlanType;
import com.tmobile.services.Family;
import com.tmobile.services.Individual;


public class GetPlanTypeFactory {
	
	public static PlanType getPlan(String planType) {
		
		if(planType.equalsIgnoreCase("individual")) {
			return new Individual();
		}
		else if(planType.equalsIgnoreCase("family")) {
			return new Family();
		}
		else {
			return null;
		}
	}
	
	public static PlanType getPlan(String planType,int typeofContract) {
		
		ContractType contractType=GetContractTypeFactory.getPlan(typeofContract);
		
		if(planType.equalsIgnoreCase("individual")) {
			return new Individual(contractType);
		}
		else if(planType.equalsIgnoreCase("family")) {
			return new Family(contractType);
		}
		else {
			return null;
		}
	}
}
