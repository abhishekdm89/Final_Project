package com.tmobile.objectsfactory;

import com.tmobile.interfaces.ContractType;
import com.tmobile.services.TwelveMonth;
import com.tmobile.services.TwentyFourMonth;

public class GetContractTypeFactory {
	public static ContractType getPlan(int planType) {
		if(planType==12) {
			return new TwelveMonth();
		}
		else if(planType==24) {
			return new TwentyFourMonth();
		}
		else {
			return null;
		}
	}

}
