package com.tmobile.restservices;

import com.tmobile.interfaces.PlanType;
import com.tmobile.objectsfactory.GetPlanTypeFactory;

public class SendResponses {
	
	public double getResult(String typeofPlan, int lines, int typeOfContract) {
		double totalCost;
		PlanType planType;
		if(typeOfContract==12||typeOfContract==24) {
			 planType = GetPlanTypeFactory.getPlan(typeofPlan, typeOfContract);
			totalCost=planType.perMonth(lines, 1000);
		}
		else {
			planType=GetPlanTypeFactory.getPlan(typeofPlan);
			totalCost=planType.perMonth(lines);
		}
		return totalCost;
	}

}
