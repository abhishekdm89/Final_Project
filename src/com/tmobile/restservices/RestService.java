package com.tmobile.restservices;

import javax.websocket.server.PathParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/")
public class RestService {
	SendResponses getResult = new SendResponses();

	@POST
	@Path("/plan")
	@Produces(MediaType.TEXT_PLAIN)
	public double getFamilyTotalBill(@FormParam(value = "typeOfPlan") String typeOfPlan, @FormParam(value = "lines") String lines, 
			@FormParam(value = "contractType") String contractType) {
		int contract;
		if (contractType.equals(null)) {
			contract = -1;
		}
		else {
			 contract = Integer.parseInt(contractType);
		}
		int numOfLines = Integer.parseInt(lines);
		
		return getResult.getResult(typeOfPlan, numOfLines, contract);
	}

//	@POST
//	@Path("/individual")
//	@Produces(MediaType.TEXT_HTML)
//	public double getIndividualBill(@FormParam(value = "deviceSelected") String deviceSelected,
//			@FormParam(value = "contarctType") int contractType) {
//		String typeOfPlan = "individual";
//		int lines=1;
//		if (deviceSelected.equals(null)) {
//			contractType = -1;
//		}
//
//		return getResult.getResult(typeOfPlan, lines, contractType);
//	}
}
