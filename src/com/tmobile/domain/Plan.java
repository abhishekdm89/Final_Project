package com.tmobile.domain;

public class Plan {
	
	private int lines;
	private String planType;
	
	public Plan(int lines, String planType) {
		
		this.setPlanType(planType);
		if(this.getPlanType().equalsIgnoreCase("Individual")) {
			this.setLines(1);
		}
		else {
			this.setLines(lines);
		}
	}
	
	public int getLines() {
		return lines;
	}
	public void setLines(int lines) {
		this.lines = lines;
	}
	public String getPlanType() {
		return planType;
	}
	public void setPlanType(String planType) {
		this.planType = planType;
	}

}
