package com.tmobile.domain;

public class Customer {
	private String name;
	private String emailId;
	private int id;
	private Plan plan;
	private Device device;

	public Customer(int id, String name, String emailId, Plan plan) {
		this.setId(id);
		this.setName(name);
		this.setEmailId(emailId);
		this.setPlan(plan);
	}

	public Customer(int id, String name, String emailId, Plan plan, Device device) {
		this(id, name, emailId, plan);
		this.setDevice(device);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Plan getPlan() {
		return plan;
	}

	public void setPlan(Plan plan) {
		this.plan = plan;
	}

	public Device getDevice() {
		return device;
	}

	public void setDevice(Device device) {
		this.device = device;
	}

}
