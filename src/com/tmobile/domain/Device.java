package com.tmobile.domain;

public class Device {
	private String model;
	private double price;
	private int typeofContract;
	private long imei;
	
	public Device(String model, long imei, double price,int typeofContract) {
		this.setModel(model);
		this.setImei(imei);
		this.setPrice(price);
		this.setTypeofContract(typeofContract);
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}
	public int getTypeofContract() {
		return typeofContract;
	}

	public void setTypeofContract(int typeofContract) {
		this.typeofContract = typeofContract;
	}

	public long getImei() {
		return imei;
	}

	public void setImei(long imei) {
		this.imei = imei;
	}

}
