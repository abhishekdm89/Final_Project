package com.tmobile.service;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import com.tmobile.interfaces.PlanType;
import com.tmobile.objectsfactory.GetPlanTypeFactory;

public class IndividualTest {
	private PlanType noDevice, planType12, planType24;

	@Before
	public void setUp() {
		noDevice = GetPlanTypeFactory.getPlan("individual");
		planType12 = GetPlanTypeFactory.getPlan("individual", 12);
		planType24 = GetPlanTypeFactory.getPlan("individual", 24);
	}

	@Test
	public void testNoDevice() {
		double expected = 70.0;
		double actual = noDevice.perMonth(1); // 1 -> Number of lines, required by method in the Interface
		assertEquals(expected, actual, 0.01);
	}

	@Test
	public void testPerMonth12() {
		double expected = 153.33;
		double actual = planType12.perMonth(1, 1000.0);
		assertEquals(expected, actual, 0.01);
	}

	@Test
	public void testPerMonth24() {
		double expected = 111.70;
		double actual = planType24.perMonth(1, 1000.0);
		assertEquals(expected, actual, 0.01);
	}

	@Test
	public void testPerMonthMoreLines() {
		double expected = 111.70;
		double actual = planType24.perMonth(100, 1000.0);
		assertEquals(expected, actual, 0.01);
	}

}
