package com.tmobile.service;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import com.tmobile.interfaces.PlanType;
import com.tmobile.objectsfactory.GetPlanTypeFactory;

public class FamilyPlanTest {

	private PlanType noDevice, planType12, planType24;

	@Before
	public void setUp() {
		noDevice = GetPlanTypeFactory.getPlan("family");
		planType12 = GetPlanTypeFactory.getPlan("family", 12);
		planType24 = GetPlanTypeFactory.getPlan("family", 24);
	}

	@Rule
	public ExpectedException exe = ExpectedException.none();

	@Test
	public void testNoDevice2() {
		double expected = 112.0;
		double actual = noDevice.perMonth(2);
		assertEquals(expected, actual, 0.01);
	}

	@Test
	public void testNoDevice3() {
		double expected = 147.0;
		double actual = noDevice.perMonth(3);
		assertEquals(expected, actual, 0.01);
	}

	@Test
	public void testNoDevice4() {
		double expected = 140.0;
		double actual = noDevice.perMonth(4);
		assertEquals(expected, actual, 0.01);
	}

	@Test
	public void testPerMonth212() {
		double expected = 195.33;
		double actual = planType12.perMonth(2, 1000.0);
		assertEquals(expected, actual, 0.01);
	}

	@Test
	public void testPerMonth312() {
		double expected = 230.33;
		double actual = planType12.perMonth(3, 1000.0);
		assertEquals(expected, actual, 0.01);
	}

	@Test
	public void testPerMonth412() {
		double expected = 223.33;
		double actual = planType12.perMonth(4, 1000.0);
		assertEquals(expected, actual, 0.01);
	}

	@Test
	public void testPerMonth224() {
		double expected = 153.70;
		double actual = planType24.perMonth(2, 1000.0);
		assertEquals(expected, actual, 0.01);
	}

	@Test
	public void testPerMonth324() {
		double expected = 188.70;
		double actual = planType24.perMonth(3, 1000.0);
		assertEquals(expected, actual, 0.01);
	}

	@Test
	public void testPerMonth424() {
		double expected = 181.70;
		double actual = planType24.perMonth(4, 1000.0);
		assertEquals(expected, actual, 0.01);
	}

	@Test
	public void testLessLinesIllegalArgument() {
		exe.expect(IllegalArgumentException.class);
		exe.expectMessage("Number of lines should be between 2 and 4");
		planType24.perMonth(1, 1000.0);
	}

	@Test
	public void testMoreLinesIllegalArgument() {
		exe.expect(IllegalArgumentException.class);
		exe.expectMessage("Number of lines should be between 2 and 4");
		planType24.perMonth(5, 1000.0);
	}

	@Test
	public void testNegativeLinesIllegalArgument() {
		exe.expect(IllegalArgumentException.class);
		exe.expectMessage("Number of lines should be between 2 and 4");
		planType24.perMonth(-1, 1000.0);
	}

}
